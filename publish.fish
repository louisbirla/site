# More at birla.io/questions/how-is-this-site-run
# Just in case git
git add .
git commit -m "backup"
# Generate the files
zola build
# Checkout to pages
git checkout pages
# Publish to IPFS (Disabled for now)
# ipfs pin rm -r (head -1 .site_cid) # Remove old site from pinning
# ipfs pin remote rm --service=pinata --cid=(head -1 .site_cid) # Remove old site from pinning service
# ipfs add --cid-version=1 -Q -r public > .site_cid # Start pinning new site
# ipfs pin remote add --service=pinata (head -1 .site_cid) # Send to pinning service
# Move build
mkdir todelete
mv * todelete/
mv todelete/.domains .
mv todelete/public/* .
mv todelete/.site_cid .
rm -rf todelete
# Add and commit
git add .
git commit -m "publish script"
git push origin pages
# Checkout back to main
git checkout main