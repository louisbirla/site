# Louis Birla

Herein lies my personal website.

Find it online: [HTTPS Site](https://birla.io)

## Welcome

If you came here from one of my websites, this is my [Git](https://git-scm.com/) repository where you can find all the code that runs the websites.

## Development

The website currently uses [Zola](https://www.getzola.org/) for generation.
