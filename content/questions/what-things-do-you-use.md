+++
title = "What things do you use?"
aliases = ["q/2"]
+++

I work mostly [:digitally](#Software), but sometimes use [:analog](#Analog) solutions.

## Software

### Obsidian
Most of my work I do through [Obsidian](https://obsidian.md/). I will write a few guides on my workflow and theory behind it, but if you value building understanding, I highly recommend it.

### Zotero
[Zotero](https://www.zotero.org/) is where I do my digital reading of PDFs and ePubs. I annotate them from within the app and use various plugins to get them into [:Obsidian](#obsidian). Zotero is mainly a citation manager, and I use it for that as well.

### Things
I use [Things](https://culturedcode.com/things/) to manage my tasks. It works as all my daily [:electronics](#electronics) are from apple, and I paid for this app a while ago. It is very polished and nice to use.

### Music
Since my family is subscribed with a family plan, I use [Spotify](https://open.spotify.com/). I value it especially because of the ability for me to discover new music. I also download music and listen to it with [Jellyfin](https://jellyfin.org/). Stalk my listens at [ListenBrainz](https://listenbrainz.org/user/louisbirla/).

### MacBook Software
My web browser of choice is [LibreWolf](https://librewolf.net/), although I do not use many of its privacy features. My browsing experience significantly improved by [Sidebery](https://github.com/mbnuqw/sidebery), which replaces my tab organization. In fact, I have removed the horizontal tabs from display.

For my programming I use [VS Code](https://code.visualstudio.com/) through [VSCodium](https://vscodium.com/). My theme of choice shifts between [GitHub](https://marketplace.visualstudio.com/items?itemName=GitHub.github-vscode-theme) and [Gruvbox](https://marketplace.visualstudio.com/items?itemName=jdinhlife.gruvbox).

[Raycast](https://www.raycast.com/) replaces [Spotlight](https://support.apple.com/guide/mac-help/search-with-spotlight-mchlp1008/mac) on my MacBook.

### iPhone Software
My phone browser is [Orion](https://kagi.com/orion/). Opening Safari to search for things was very slow, but Orion is quite responsive.

I am an avid listener of podcasts, and do so through [Snipd](https://www.snipd.com/). I do use its snipping features, which I upload to [:Obsidian](#obsidian), but the interface is also really good!

## Analog
I used to use [:paper](#notebooks) and [:pens](#writing) for most of my affairs. I still use them occasionally, but my work now is mostly digital.

### Writing
I use a charcoal-colored [Lamy Safari](https://www.lamy.com/en/lamy-safari) with a fine nib for most of my writing. I am not an ink connoisseur, and use a [Black Document Ink](https://www.de-atramentis.com/en/document-ink--2509.html).

I usually write with only one color, but occasionally I will use three [TUL Gel Pens](https://www.officedepot.com/a/products/191888/TUL-Retractable-Gel-Pens-Medium-Point).

<figure>

![red, blue, and green tul gel pens resting on an index card](tul_gel_3.png)
</figure>

I'll use a whiteboard for working problems out or mapping concepts that would require much iteration{% sidenote() %}rather, working things out that would require a lot of erasing{% end %}. These days, I often use Obsidian's Canvas feature instead.

I have ultra-fine dry erase markers in black, blue, red, and green. Their writing size is great, especially for the small cards I write on. I also have fine wet-erase markers of the same colors.

### Notebooks
My main notebook at the moment is a [classic medium-sized Leuchtturm1917 notebook](https://www.leuchtturm1917.us/notebooks/all-formats/medium-a5/), which I highly recommend. I much prefer it to the moleskin I used to use.

<figure>

![the moleskine notebook, open to a blank spread of lined paper. there is ghosting from the previous page, unintelligible writing in black ink](notebook.png)
</figure>

I tend to think of my [:voice recorder](#Voice-recorder) as a notebook as well. It is great for stream-of-consciousness, or for recording things on the go!

## Electronics
I mostly use two devices: a [:Macbook](#Macbook) and an [:iPhone](#Iphone), although I have two other laptops, one for work and one to run programs for my classes.

### MacBook
I use a [2020 M1 MacbookPro](https://support.apple.com/kb/SP824).

<figure>

![a space-gray macbook, closed shut. the metallic apple logo has a few scratches](macbook.png)
</figure>

### iPhone
I use an [iPhone Xs Max](https://support.apple.com/kb/SP780).

### Voice Recorder
I use a [Sony IC Recorder](https://electronics.sony.com/audio/walkman-digital-recorders/audio-digital-voice-recorders/p/icdux570blk) as my primary tool for expressed thinking. I will write more about my process, but this device has catalyzed my realization of many of my *real* passions.

### Earbuds
I use [Beats Fit Pros](https://www.beatsbydre.com/earbuds/beats-fit-pro) on a daily basis, received as a very generous present.
