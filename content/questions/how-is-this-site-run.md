+++
title = "How is this site run?"
aliases = ["q/1"]
+++

The site is open source, so you can see for yourself [here](https://codeberg.org/louisbirla/site)!

It uses [Zola](https://www.getzola.org/) to generate static files which are run by [Codeberg Pages](https://codeberg.page/). Zola lets me write [markdown](https://www.markdownguide.org/getting-started/) and use [templating](https://css-tricks.com/what-ya-need-there-is-a-bit-of-templating/), which lets me use building blocks to make the website great.

During development, I run `zola serve` and simply change the files. Once I'm done making changes, I'll commit, push the changes, and run [a script](https://codeberg.org/louisbirla/site/src/branch/main/publish.fish) that [:publishes the changes](/questions/what-is-in-the-publish-script#Whatisinthepublishfishscript).

The site's styling is almost entirely [Tufte CSS](https://edwardtufte.github.io/tufte-css/). This is what allows sidenotes{% sidenote() %}Like this one{% end %} and various other niceties. The links with dotted underlines are powered by [:Nutshell](https://ncase.me/nutshell/#WhatIsNutshell), a really cool project that allows me to link together parts of my site. Crucially, the links work as normal when there is no javascript enabled.
