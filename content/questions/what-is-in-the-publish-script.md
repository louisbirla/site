+++
title = "What is in the publish.fish script?"
aliases = ["q/1a"]
+++

[This script](https://codeberg.org/louisbirla/site/src/branch/main/publish.fish) publishes my website. It's written in [fish](https://fishshell.com/), my shell of choice.

## Walkthrough
Here's what each part of the script does:

### Auto-Commit

```
git add .
git commit -m "backup"
```

These two lines ensure that any changes I haven't committed aren't lost.

### Build

```
zola build
```

Zola makes this really easy, just one command to build the static output. All the files are in a new folder called `public`.

### Codeberg Pages

Codeberg Pages serves my site at birla.io. Using this is very convenient since I just need to push the static pages to the `pages` branch of the repository.

```
git checkout pages
```

First, I switch to the `pages` branch. The problem is that Codeberg Pages expects the files to be at the root of the repository, not in the `public` folder we have at this point.

```
mkdir todelete
mv * todelete/
mv todelete/.domains .
mv todelete/public/* .
mv todelete/.site_cid .
rm -rf todelete
```

These commands come next{% sidenote() %}Technically, the IPFS steps come next.{% end %} to replace the current build at the root level with the new one:

1. Make a temporary folder.
2. Move the both the current build and the `public` folder to that folder.
3. Bring the content of the `public` folder and a few other relevant files{% sidenote() %}`.domains` for Codeberg Pages and `.site_cid` for IPFS{% end %} out of the temporary folder.
4. Delete the temporary folder.

The last step to publish the site is to commit and push the changes to Codeberg:

```
git add .
git commit -m "publish script"
git push origin pages
```

The *actual* last step is to go switch back to the `main` branch:

```
git checkout main
```

### IPFS

[I'm not a fan of "web3."](https://yesterweb.org/no-to-web3/) I have fallen for the excitement of cryptocurrency and blockchain in the past, and am now wiser of it. [IPFS](https://ipfs.tech/) is web3-adjacent, but the technology is open-protocol{% sidenote() %}If it is not, please tell me.{% end %} and free.

I love distributed/peer-to-peer technology, and IPFS is a widely adopted technology of this kind. It's actually really cool, I think.

Since the entire site is static, it can be distributed easily with IPFS. The following two lines keep my site available to access:

```
ipfs pin rm -r (head -1 .site_cid)
ipfs add --cid-version=1 -Q -r public > .site_cid
```

The [CID](https://docs.ipfs.tech/concepts/content-addressing/) of the site is saved in the file `.site_cid`. The first line stops [pinning](https://docs.ipfs.tech/how-to/pin-files/) the old site, and the second line starts doing so.

Notes: {% marginnote() %}I also have pinning set up for the pinata pinning service.{% end %}
- I stop pinning old versions since I push changes frequently.
- `-r` means I pin every file involved.
- `--cid-version=1` lets me use the newer CIDs
- `-Q` tells `ipfs` to output just the CID, which `> .site_cid` saves to the file.
- IPFS isn't a good fit for large files, but for the web and many smaller files, I think it is great.

A happy accident of the publishing script is that the CID for the rest of the site is published online. One part of the script on `base.html` is to check for that file and if it exists create a link to the IPFS site at the bottom of each page.
