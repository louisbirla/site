+++
title = "What Haiku have you composed?"
aliases = ["q/3"]
+++

While I enjoy composing them, my Haiku are not the crux of my productive effort. As they are tiny in size, they may be put on display with less difficulty than essays or books.

## Haiku
Here you will find a few of my haiku, a definition I take lightly---emotional short-form poetry. Throughout the evolution of this website I have been more and less selective about what I include; so I may remove those I find distateful and you may archive those you do not.

{% epigraph(author="Louis Birla", work="A Lesson Happily Learned") %}
The trees dance to the singing wind,<br />
but I hear the cicada-<br />
familiar sound.
{% end %}

{% epigraph(author="Louis Birla", work="A Painting from Nature", date="16 July 2021") %}
The Moon can see my room,<br />
casting shadows on the ceiling.<br />
Indoor scenery
{% end %}

{% epigraph(author="Louis Birla", work="In the palm of my hand...", date="6 November 2022") %}
Gazing at the stars,<br />
my right hand can hold them all.<br />
I am excited.
{% end %}

{% epigraph(author="Louis Birla", work="When they are connected", date="21 February 2023") %}
In a sky of gray clouds<br />
I can still see the stars that<br />
I keep in my hand
{% end %}

{% epigraph(author="Louis Birla", work="A Romantic Affair", date="6 February 2023") %}
Park bench.<br />
Bird sings futile song<br />
for my ears alone.
{% end %}

{% epigraph(author="Louis Birla", date="6 October 2024") %}
Summer wanes<br />
The sound of the fan occasionally takes my attention<br />
My heart beats fast,<br />
but I am not sweating<br />
Not that much,<br />
anyway.
{% end %}
