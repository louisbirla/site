+++
title = "Budr Basef"
date = 2023-05-28
aliases = ["e/1a"]
+++

"Budr Basef" is a game I made in 2018, which I describe as "ERS on Crack." I'll explain the rules from scratch, so if you are not familiar with ERS, you don't need to look it up.

The game itself is a chaotic, hand-hurting, brain-rending game that includes the following features:

- 15+ patterns to memorize
- gambling (non-financial)
- sleight-of-hand
- card counting
- politics

It's definitely not a game meant to be played in a competitive sense (it could be modified to fit such a context). I encourage cheating, as long as you aren't caught. The ultimate goal is to have fun.

## Lay of the Land

*Land = playing surface

This game is meant to be played with a deck of cards. If you want to play with more cards, you'll probably have to modify the rules.

### Gameplay Loop

The loop itself is quite simple. You hold on to some cards, called a **deck**.
On your turn, you must take a card from your deck and put it face up on a collective pile called the **stack**. Then, it is the next player's turn.

At any point, you may **hit** the stack, by touching the playing surface/your thigh, then the stack. If the cards in the stack match against predefined **sets**, then you take all the cards. If it isn't then you have to put the next 10 cards from your **hand** onto a pile next to the stack, called the **keep**.

(On a successful hit, you take the cards in the stack and the keep)

### Winning & Losing

Once a player has every card in their deck, they win the game. The game has no "losers" in the sense that everybody who does not have any cards can still join in the game. Players without cards can still *hit*, only they must use a different sequence to do so.

## Hitting

For players with a deck, a hit requires touching the deck, a surface, and the stack in that order.
For players without a deck, it requires touching their carry hand, forehead, a surface, and the stack in that order.

A player's hands have specific roles. It doesn't matter which one is which, they can even switch during a game. However, a single hand cannot be both at the same time.

One hand is the **carry hand**, and has the role of holding the deck. The other is the **striking hand**, which does the hitting.

If multiple players try to hit around the same time, the first is the one who *actually* hit, the rest have not. For that player, the hit is successful if the stack matches a set (described below), and unsuccessful otherwise.

When a player successfully hits, they take the cards from both the stack and the keep, and add them to their own deck. Shuffling optional. When a player unsuccessfully hits, they must put 10 cards from their deck into the keep. The game keeps going.

### Sets

If the top card of a hit is a Joker, it is not a set. Hitting it is unsuccessful, since playing a Joker goes to Gambling.

Ace = 1, Jack = 11, Queen = 12, King **is not** 13. King is not a number.

- **Twelve**: The top two cards add to exactly 12. (e.g 7+5, J+A)
- **King**: The top card is a King and it is not the only card in the stack.
- **Double**: The top two cards are the same number.
- **Fries**: The (absolute) difference between the top two cards is 1, and they are of the same suit. (e.g 2s+3s)
- **Genesis**: The top card and the first card are the same number.
- **Sandwich**: The top card and the 3rd card from the top are the same number.
- **Odd**: The top 3 cards are all odd.
- **Even**: The top 3 cards are all even.
- **Thirteen**: The top 2 cards add to exactly 13, and they are the same color. (e.g Js+2c)
- **Suit**: The top 3 cards are all of the same suit.
- **Straight**: The top 3 cards are in intervals of exactly 1, either descending or ascending. (e.g 7+8+9)
- **Sprite**: The top 3 cards are in intervals of either 1 or 2 and are all the same color, either descending or ascending. (5s+7s+8c)
- **Dragon**: Of the top 3 cards, one divides into another to make the other when rounded down. (e.g J+2+5)
- **Color**: The top 5 cards are of the same color.
- **All**: All cards are in the stack.

## Gambling

When the Joker card is played (top card of the stack), the game changes to gambling mode. To enter into this mode follow the following steps:

1. Combine the stack and the keep to form the **pool**.
2. Each player looks discretely at the top card of their decks and sets it aside. That card is the **power**.

After this, the rounds of gambling begin, turn-by-turn starting with the player who played the Joker.

### Turn

On a turn during gambling, players have the following actions available:

- **Fold**: Put the power into the pool. Players who fold do not have any turns until gambling is over.
- **Raise**: Put any number of cards into the pool from the deck. The other players must put that many cards into the pool or fold.
- **Check**: If you do not owe any cards into the pool, you can check. If, by your turn, every player has checked, gambling continues to the reveal phase.

### Reveal

All players (that have not folded) reveal their powers. The highest power takes the entire pool, including all the powers. Those cards go into the deck, where the usual game proceeds.

This is the order of cards, highest to lowest: Joker > King > Queen > Jack > 10 > .. > 2 > Ace. In case of a tie, continue with Hearts > Spades > Clubs > Diamonds.

## Variations

There are a few variations on the game that can twist it to make it easier or harder. Both make it more chaotic, though.

### Free-style

In this variation, play begins with only the "King" and "All" sets. Whenever a player hits successfully on a King, they add additional set rules, or modify existing ones, except the King.

### Announce

For this variation, any hit requires the player to declare the name of the set while hitting. Instead of chronologically, the player with the declared set ranked lowest on the list actually hits. If the stack does not match the set declared, it is not successful.

## Funny Story

I was playing this game during school lunch with a few friends. At this point in time, every hit required hitting the forehead.
I was picking up stacks after stacks, and one of my friends was really close after me. The stack displayed 6 and 7, both black. It didn't connect in my brain, and my friend went straight for it, crushing his glasses on the way there.

I hope you will play this game and encounter similar "fun" experiences!
