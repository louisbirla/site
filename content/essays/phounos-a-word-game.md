+++
title = "Phounos: A Word Game"
date = 2023-10-27
aliases = ["e/1"]
[extra]
pdf = true
+++

I have a moral obligation to accompany the following description of Phounos with a warning: While Phounos is a game meant to be played for fun and leisure, it could have an adverse effect on a particular subset of competitive people: those who refuse to lose. The majority of the game consists of intelligent wordplay, yet it can{% sidenote() %}and, on occasion, should{% end %}devolve into barbaric argumentation at critical points. It is a perfect "test of patience," and therefore a valid method for developing the virtue.

This is the first essay I've written outside of class. It's not even academic---but to me, that's not what an essay means. The word *essay* comes from the French "Essayer," or *to attempt*. I want to *try* to show you something, and if I fail to, I'll consider giving it another go in the future. For now, I hope you enjoy.

I like stories. I also like a structured "dumping" of facts, but I tend to prefer stories. If you are like me, go ahead and read on; I have a story to tell. If you'd rather get that fact dump of rules, skip to the "The Rules" section.

## How to Play

I won't be giving you your usual guide to playing this game{% sidenote() %}You can see how the game is played in the examples and [:Rules](#TheRulesDeclaratively) section below.{% end %}, instead I will tell you a story of how the rules were formed. This will make it more intuitive to understand (I hope), and in the process, you may find value out of the method and invent games of your own! If you do, please share them with me, I'd love to playtest.

If you've ever been with siblings or friends who know you well, you've probably found yourself awkwardly brainstorming of what to say once finished talking about everything in the world. We will develop Phounos as an activity that could fill that time---a game that could be played anytime, anywhere. To that end, Phounos would have to be played with naught but one apparatus: voice. There are many games that fit this niche, such as *20 questions* and *would you rather*, and they are great. However, they lack a competitiveness to them. They lack a *reason* to play, other than to fill a void. Because of that, they can be quite dull. Phounos would take a different approach, pitting players against each-other and having fun with the bloodbath that ensues.

Our starting ground is pretty bare. We have two criteria: the game must be *oral* and *competitive*, but how do we proceed? Drawing inspiration from other works never fails to give a sense of direction. In the past I've come up with many tiny games where each player gets a turn to speak and must say the right things, or lose. Among this collection is *memory list*, where players take turns saying every word that has been said before, in order, adding a new one each time. I liked the gameplay loop of picking words to try to confuse and trick opponents, let's stick with that motif designing Phounos. Let's find a way for each turn to be simple, requiring nothing more than a single word from the player.

Let's see how our game plays:

- Player 1: "House"
- Player 2: "Mouse"
- Player 3: "Bat"
- Player 1: "Ice cream"
- Player 2: "Mouse"
- Player 3: "This game sucks."

Oh. Looks like the game wasn't very fun. Well that makes sense, since any fun activity requires an aspect of difficulty. Picking and saying aloud one out of the thousands of words isn't challenging, and Player 2 even repeated a word. We can increase the difficulty a bit by requiring every word to be new, but that won't be difficult enough until thousands of words have been spoken. How about making each word related to the last? That way, players can be strategic and choose words that don't have many relations in attempt to sabotage future players. Let's play!

- Player 1: "Apple"
- Player 2: "Mango"
- Player 3: "Orange"
- Player 1: "Red"
- Player 2: "Blue"
- Player 3: "Green"
- Player 1: "I know 1000 more colors, this game could go on forever."

This playthrough went better than the last one, but still didn't seem difficult enough. I noticed something really cool, maybe you did as well---at first the players were using words that were related through their shared fruithood, then the connection shifted to color words. The junctions there was "orange," which fit both of those categories. What would happen if we continued exploring that mechanic? Every word must be a *homophone*, a sound associated to multiple meanings.

- Player 1: "Orange"
- Player 2: "Blue/Blew"
- Player 3: "Red/Read"
- Player 1: "Persimmon"
- Player 2: "Curry"
- Player 3: "I don't think curry has multiple meanings."

This time around, the game is more challenging. It also seems that it went well until Player 3's complaint. The game halted, and since we don't have a way to address that, or for the game to end anyway, let's squeeze some brain juice. Player 2 must think that "curry" has multiple meanings and that it's related to "persimmon," since those are the rules of the game, but Player 3 doesn't think it has more meanings than that delicious type of food. Since there is a dispute between players, who should win in this case? Our criteria is that every word must have multiple meanings, so does curry? Off the top of my head, I know that to *curry* is a term used in programming, distinct from the food. So I'll give the win to Player 2!

- Player 1: "Hey, what about me?"

Oh, I guess if we say that Player 2 won the game just because Player 3 was wrong, that wouldn't be fair to Player 1. How about instead of a player winning, we had their counterpart in the argument lose. Last player standing wins! Alright, one more time.

- Player 1: "Orange"
- Player 2: "Blue/Blew"
- Player 1: "Red/Read"
- Player 2: "Persimmon"
- Player 1: "Curry"
- Player 2: "Cheese"
- Player 1: "Wine/Whine"
- Player 2: "Tripe"
- Player 1: "Nah, tripe only has one meaning."

Hmm, it seems like the beginining of the game was exactly the same as the last one. More important than that, Player 1 had an issue with Player 2, this time over the word "tripe." I feel like there would be another meaning, let me check the dictionary...

- Player 1: "Hold on, *we don't have a dictionary*."

That's right! One of the two constraints for Phounos was that it only needed a voice{% sidenote() %}and a brain, and a few other prerequisites{% end %} to be played. How can we decide who wins without either pre-deciding every case{% sidenote() %}Players couldn't have an entire list of homophones to reference.{% end %} or needing players to consult a dictionary? Debate! Have the players figure it out themselves. If the players agree that a word has multiple meanings, they can resolve the conflict themselves.

Additionally, remember what led us to make the homophone rule? That was the surprising switching from category to category, but the last rounds were stuck in one category. Let's keep that in mind as we play a few more games.

- Player 1: "Bear/Bare"
- Player 2: "Bat"
- Player 1: "Fish/Phish"
- Player 2: "Procure"
- Player 1: "Procure isn't a homophone."
- Player 2: "Yes it is."
- Player 1: "What are it's meanings?"
- Player 2: "In one sense, you get a an item, like the Fish. In the other sense, you get something to happen."
- Player 1: "That's the same thing; getting what you want."
- Player 2: "No"
- Player 1: "Yes"
- Player 2: "No"
- ...
- Player 3: "I think Player 1 is right."
- Player 2: "2 against 1, I'm right"
- Player 1: "Don't we have to be unanimous?"

Yet another issue! This time, two players disagreed on the validity of "procure," and the third player seemed convinced one way. Player 1 thought the decision had to be unanimous, but Player 2 thought it just had to be a majority. Which one should it be? In this scenario, Player 1 wouldn't concede that "procure" had multiple meanings, but likewise Player 2 wouldn't concede that it only had one. The voting would never be unanimous, so majority rule it is!

I also noticed a switch in categories with "fish," from animal nouns to a verb. Let's look at it closer: "bear" was related to "bat" as both are animals, "bat" to "fish" as animals, then "fish/phish" to "procure" as the act of *fishing* or *phishing*. What if every word had to be related to the previous word in a sense(s) that *wasn't* used to relate to the previous? Sounds confusing, but then "fish/phish" would no longer work, since "bat" (animal) was related to "bear" (animal), and to "fish" (animal). A word that would work in it's place, though could be "stick," as both sticks and (baseball) bats are often wood.

In the previous round, majority ruled Player 2 out, so Player 1 won! Let's play again, with our rule updates.

- Player 1: "Our/Are/R"
- Player 2: "Be/Bee/B"
- Player 3: "Flower/Flour"
- Player 1: "Bread"
- Player 2: "Dough"
- Player 1: "Hey, Dough is related to Bread in the same sense that Bread is to Flour!"
- Player 2: "True, but they are *also* connected in the sense that both Bread and Dough are slang for money, while Flour isn't."
- Player 1: "And that's OK? I thought Dough *couldn't* be used because it was also related to Flour."
- Player 3: "It's OK, since the *money* connection wasn't used between Flour and Bread."
- Player 1: "Alright, I see. I'm out then."

Wow, that was great! Now, Players 2 and 3 play it out.

- Player 2: "Second"
- Player 3: "Our/Hour"
- Player 2: "We/Wee"
- Player 2: "Kid"
- Player 3: "Barbecue"
- Player 2: "Hey, that doesn't have multiple meanings!"
- Player 3: "Yes it does, it's a noun for a type of food, but you also use a barbecue to do it."
- Player 1: "Yeah, it's true."
- Player 2: "Oh. So I lose?"
- Player 3: "Hahaha!"

So Player 2 challenged Player 3 over the word "barbecue," saying that it doesn't have multiple meanings, and lost. However, Player 3 should have lost, since "Barbecue" isn't related to "Kid!" The problem here was that there aren't any rules on how a dispute should be handled. Let's have any player be capable of challenging a word just played, by doing nothing more than exclaiming "Challenge!" Then, the word-sayer should explain the meanings of their word and how it is properly related. This way, Player 3 can't get away with "barbecue" so easily.

- Player 3: "Hold on, you joke around at the barbecue though."

Sure...

- Player 3: "Another word for *joke* is *kid*."

So you're making the case that "kid" and "barbecue" are related?

- Player 3: "Yep."

Looks like anything could be related, at least since any word you say is going to be made of letters. Both "kid" and "barbecue" have a vowel and some consonants in them! They must be related! No, let's draw a line somewhere.{% marginnote() %}What does and doesn't count as related is a real pont of debate in this game.{% end %} How about using a test, like putting the words in a sentence? If you have to introduce more than one concept to go from one to another, they're not closely related. That still doesn't stop relations like "they're both words," so the relation has to be in the meaning of the word, not what it looks like or how it is composed.

Player 3 won last game, so let's start over.

- Player 1: "Tee/Tea"
- Player 2: "Club"
- Player 3: "Group"
- Player 1: "Set"
- Player 2: "Cause"
- Player 1: "Challenge!"
- Player 2: "Cause as in to instigate, and Cause as slang for *because*. It's related to Set in the sense of preparation, Set is related to Groups in mathematics."
- Player 1: "But you pronounce Cause as *cuz*, not Cause."
- Player 3: "Well I pronounce it as Cause, exactly like I do the end of *because*. *Bee-Cause*."
- Player 1: "Sure, but Player 2 pronounces it as *cuz*. *Bee-Cuz*".

How could we have made a game based on pronunciations when people pronounce things differently?! Well, we have to make a decision. People shouldn't pronounce things the way they usually wouldn't. In this case, Player 2 would lose the challenge, but if Player 3 had said the same word in the same context, they would have won it.

- Player 2: "That's not fair, though."

I guess not. Moving on, let's keep playing, this time with players 1 and 3.

- Player 2: "Hold on, why am I completely out of the running because of one mistake? At least give me a few chances."

With a game this easy to mess up{% sidenote() %}And from the play-testing I have done{% end %}, it makes sense to give people a few chances per game. We can split a single game into rounds, each round ending with a challenge and somebody getting a *strike*. A player's out of the running when they reach 3 strikes, but can still vote for who they think won a challenge.

Player 2 now has 1 strike, two more and they're out.

- Player 2: “Who will start now?”

Every turn after the second becomes a tiny bit harder, but essentially the same difficulty. The first player's word doesn't have to be related to anything, and the second can comfortably choose any relation to the first. Let's give some leniancy to the player who lost the challenge by letting them start. Then, reverse the order of play every round to shake things up. Player's shouldn't always go after a particular player, who's vocabulary may be in a completely different world.

- Player 2: "Wood/Would"
- Player 1: "Plant"
- Player 3: "Factory"
- Player 2: "Code"
- Player 1: "Crack"
- Player 3: "Break/Brake"
- Player 2: "End"
- Player 1: "Tip"
- Player 3: "Wage"
- Player 2: "Prompt"
- Player 1: "Ask"
- All: “…”
- Player 3: "Challenge."
- Player 1: "You Ask a question, like a Prompt, and you can trigger something, like Prompting something, when you engage, or Wage."
- Player 3: "What's the other meaning of Ask?"
- Player 1: "Oh, it's my friend's name."

Stop right there! Let's talk about proper nouns. Where should we draw the line? At minimum, let's ban names of people, since people can have all sorts of names. Place names are also really varied, so let's ban those.

Player 1 wins the challenge, Player 3 gets a strike.

- Player 3: "Class"
- Player 1: "School"
- Player 2: "Dunk"
- Player 3: "Submerge"
- Player 1: "Heal/Heel"
- Player 2: "Challenge for Submerge."
- Player 3: "Hey, Player 1 already said their word."

To keep it simple, only the latest word can be challenged.

A strike for Player 2. One more and they're out.

- Player 2: "Float"
- Player 1: "Blew/Blue"
- Player 3: "Challenge!"
- Player 1: "Blew with breath, and Blue as the color. You can blow up a float."
- Player 3: "No, parade floats aren't balloons or anything."
- Player 1: "Pool floats can be blown up."
- Player 3: "Anyways, isn't Blue a proper noun?"

Technically, colors are generally considered common nouns. You can tell because they aren't capitalized.

- Player 3: "Ok, so words like March would be proper nouns."

Months are proper nouns, should they be accepted? Some of the words could work, like "March" and "May," but is that enough to justify their inclusion? There are going to be a lot of proper noun edge cases like this, aren't there? Let's disallow the use of proper nouns. No names for things that are usually capitalized.

Player 3 with 2 strikes total.

- Player 3: "Sale/Sail"
- Player 1: "Seller/Cellar"
- Player 2: "Wine/Whine"
- Player 3: "Challenge!"
- Player 2: "Huh? What's wrong now? Wine as in the drink, Whine as in the act. A Seller puts things on Sale, while a Cellar is full of Wine."
- Player 1: "Yeah, what's wrong?"
- Player 3: "Haha. Player 1 already said Wine/Whine before."
- Player 1: "Really? Maybe..."

Yeah, one of the previous rules we had was that words shouldn't repeat. Looking at our game now, we didn't decide if that rule was meant to be applied to the entire game, or just it's rounds. The players can barely remember that they already used that word this game, so let's allow repeated words across rounds, but not within them.

- Player 3: "Noooo."
- Player 2: "Sounds good to me!"
- Player 1: "Same here."

With this, Player 3 gets a strike and is out of the game. It's Player 1 against 2, and I have a feeling with this the game will come to an end.

- Player 2: "Hey!/Hay"
- Player 1: "Cereal/Serial"
- Player 2: "Killer"
- Player 1: "Great/Grate"
- Player 2: "Cheese"
- Player 1: "Trick"
- Player 2: "Fall"
- Player 1: "Challenge!"
- Player 2: "Dang it. I know I lost this one, Fall is a proper noun."

With that, Player 2 is out and Player 1 is the victor!

- Player 1: "This is a great game{% marginnote() %}It is indeed a good game, as we all <strong>very different people</strong> agree.{% end %}!"

Why thank you! I encourage you to try this game out with your friends when you need something to kill time. I've gotten good feedback for this game, which is why I decided to write this essay. For a definitive ruleset, see the section below, but it's in good spirit to figure things through trial and error, like we did in this section.

I made this game for myself, but feel free to make it yours! If you are still reading this, I will be coming back in the future with more writing. I hope you'll come back as well and read that too! See ya! -Louis.

## The Rules, Declaratively

1. The *players* are the people who have agreed to play the *game*.
2. A number of *strikes* is associated with each player.
   1. A player with three *strikes* is said to be *out*.
   2. A player that is not out is said to be *in*.
   3. Only players that are in may recieve strikes.
3. The game consists of *rounds* of play.
   1. The number of rounds in a game varies.
   2. The player who has most recently recieved a strike *begins* the round.
      1. In the case where no players have recieved strikes recently, the player who begins the round may be decided in any manner every player agrees to.
   3. The *order of play* is the order in which players will take *turns* in the round.
      1. The order of play shall not vary in a game, except for being reversed.
      2. Every round, the order of play is reversed.
   4. After a player's turn is over, the the turn of the player next in the order begins.
   5. On a player's turn, the player must *declare* a *word*.
      1. A word is a lose definition and refers to a sound that is associated with at least one definition.
         1. The word must be pronounced by a speaker the same way the speaker would pronounce it ordinarily.
      2. A player may speak during their turn, but must make it obvious what the word they declared is.
      3. A player may not refuse to declare a word, and in doing so is immediately out.
      4. A player may not refuse to repeat or clarify a word they declared, and in doing so is immediately out.
   6. At any point during the game, any player may *challenge* the declared the word immediately prior.
      1. Words declared before the most recent declaration may not be challenged.
      2. One or no player *loses* the challenge.
         1. Only the player who initiated the challenge or the player who declared the challenged word may lose the challenge.
      3. The word must be deemed either *valid* or *invalid*.
         1. If the word is deemed valid, the player who initiated the challenge loses the challenge, and the round ends.
         2. If the word is deemed invalid, the player who declared the challenged word loses the challenge, and the round ends.
         3. If there is no consensus and the word is deemed neither valid nor invalid, no player loses the challenge and the round *ends*.
      4. When a challenge is initiated, the player who declared the challenged word must explain their word's *validity*.
         1. All players may discuss but must ultimately decide whether the word is valid, invalid, or neither.
         2. Once each player has decided whether the word is valid, invalid, or neither, the majority ruling (valid, invalid, or neither) is deemed.
            1. In the case of a majority tie between valid and neither, the word is deemed valid.
            2. In the case of a majority tie between invalid and neither, the word is deemed invalid.
            3. In the case of a majority tie between valid and invalid, the word is deemed neither.
         3. A player may be incorrect in the defence of their word's validity.
      5. When a player loses a challenge, they are assigned a strike.
   7. A round is *over* when it has ended.
      1. When a round is over and the game has not been won, a new round begins.
4. Once there is exactly one player in (and therefore the other players out), that player who is in *wins* the game.
5. Word validity
   1. A word is invalid if it is not a *homophone*.
      1. A homophone is a word with multiple meanings.
   2. A word is invalid if it is a proper noun.
   3. A word is invalid if it has already been used in the round.
   4. A word is invalid if it is not *related* in meaning to the previous word.
      1. A word is related to another if the meanings share a concept, or are shared in a concept.
   5. A word is invalid if it uses only the same relation the previous word used to it's previous word.
      1. A word that relates to the previous in both a new way and the same way that previous word was related to it's precedent, and is not otherwise invalid, is valid.
   6. If a word is not invalid, it is valid.

---

Please [:contact me](/contact#MessagingandOnlineAccounts) if you have any comments or questions. I'll clarify it both to you and in the document, adding/not adding credit to you, depending on if you want it or not.
