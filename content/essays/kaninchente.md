+++
title = "Mr. Kaninchente"
date = 2024-07-20
description = "A dialogical essay prioritizing understanding over knowledge, and justification over truth."
+++

<style>
p strong {
    font-variant: small-caps;
    opacity: 0.8;
}
</style>

<small>Find this as <a href="https://philpapers.org/archive/BIRMKM-4.pdf">a PDF</a> or <a href="https://essays.birla.io/p/mr-kaninchente">on Substack</a>.</small>

*Alice and Hugo are approached by… something… on their regular morning walk.*

**Mr. Kaninchente:** Good day! Ah, I know you two—Alice and Hugo, are you not?

**Hugo:** Hello sir. I’m not Alice, I’m Hugo.

**Alice:** And I’m not Hugo, but Alice.

**Mr. Kaninchente:** And I’m Mr. Kaninchente, but please call me Mr. K. I’ve heard that you two are quite knowledgeable, so I’d like to ask you a question.

**Alice:** Please, we love questions!

**Mr. Kaninchente:** Wonderful. What do you think I am? Of course, you two are human. With me, things are less cut-and-dry. A bit more fused-and-wet, shall we say?

<figure>
{% marginnote() %}“Kaninchen und Ente” (“Rabbit and Duck”) from
the 23 October 1892 issue of <i>Fliegende Blätter</i>
(cropped){% end %}
<img src="/essays/kaninchente/duck-rabbit.png" />
</figure>

## Belief and Disagreement

**Hugo:** Doesn’t seem that way to me, Mr. K. You are quite obviously a rabbit! Clear as day.

**Alice:** Clear as mud! Mr. K is a duck. See: that is his beak, those are his head-feathers.

**Mr. Kaninchente:** I see. One of you believes I am a rabbit, and the other a duck. With such different answers, how can you be so sure?

**Hugo:** I believed your duck-ness pretty strongly, but after hearing Alice point out another possibility, honestly, I can only be a little less confident in my own hypothesis.

**Alice:** As for me, I will do no such reduction. I even pointed out the reasons for my hypothesis, you will recall.

**Hugo:** I’ve got reasons too—I just didn’t express them! Where you see a beak I see ears, and where you see feathers I see a snout.

**Mr. Kaninchente:** Perhaps, to convince each-other or find a different answer altogether, you must both consider additional justifications.

**Alice:** I agree. Let us continue our walk, and discuss in the meanwhile.

**Mr. Kaninchente:** Mind if I walk alongside?

**Alice and Hugo:** Not at all!

**Alice:** What is to my right, Hugo?

**Hugo:** A pond.

**Alice:** What animals would you expect to find in or around a pond?

**Hugo:** Frogs, fish, ducks… I think I see where you are going with this.

**Alice:** You wouldn’t expect to find rabbits near a pond, right?

**Hugo:** No, I wouldn’t.

**Alice:** Since we're more likely to find a duck than a rabbit at a pond, and we are at a pond, Mr. K is more likely to be a duck than a rabbit. Case closed.

**Hugo:** I don’t believe so. Even if your justification is true, it’s not so relevant to our situation. After all, Mr. K can talk! He’s a classic case of an outlier.

**Alice:** Well, Mr. K? What’s the truth?

## Truth and Knowledge
**Mr. Kaninchente:** That is what I would like to know, Alice. Both of you either believe I am a duck or a rabbit, but I am not so sure myself.

**Hugo:** How do you mean?

**Mr. Kaninchente:** I’ve asked human biologists, you see. They recalled and applied their definitions—I didn’t fit the definition of either animal, so they deemed me to be neither. I myself tend to identify as both! After all, I am both rabbit-like and duck-like.

**Alice:** Then what’s the truth? The biologists understand it one way, Hugo and I another two ways, and Mr. K yet a fourth.

**Hugo:** Hmm. When you made your case for Mr. K’s duck-ness, my picture of the situation got clearer. What is that, if not getting closer to the truth? Perhaps the truth is simply what is believed the most.

**Alice:** Now that does not sound right at all. Truth is the fact of the matter, not some opinion, no matter how convincing.

**Hugo:** Would it be right to call that knowledge?

**Alice:** No. Answer me this: if there were no humans, would planet Earth still be round?

**Hugo:** Certainly.

**Alice:** That is a fact of the matter. It doesn't depend on any human. But to *know* that the Earth is round requires a *knower*.

**Hugo:** So knowledge requires somebody to do the knowing.

**Alice:** Correct. The real question is, what could we know, and how?

### Doubt
**Hugo:** I am intrigued by that question. What are we sure of? I, for one, am sure that I woke up this morning. On the right side of the bed, too!

**Alice:** Another question: if you woke up, you wouldn’t currently be dreaming, correct?

**Hugo:** Certainly. And if I didn’t wake up, I would be dreaming. But I distinctly remember waking up, so I’m not dreaming.

**Alice:** I wouldn't be so sure—haven’t you dreamt of waking up before?

**Hugo:** I have, in fact. Quite surreal.

**Alice:** If you think you are awake because you remember waking up, but also remember waking up within a dream, can you really say that you know you’re awake?

**Hugo:** I suppose that believing I’m awake just because I remember waking up doesn't hold water.

**Alice:** Right. More generally, since you could dream of many things, just because you notice them doesn’t mean they actually exist.

**Hugo:** Well, then what can I be sure of? I’ve dreamt of many things, after all.

**Alice:** I know of one thing I can be completely sure of: my experience. Maybe what I experience doesn’t exist at all as I think it does, but I cannot doubt having the experience. I’m not so sure about anything else.

**Mr. Kaninchente:** But surely you, like I, know I exist?

**Alice:** Honestly, not quite. Like Hugo, I could be dreaming. I don’t have complete certainty that you exist outside my experience. That being said, I believe in your existence.

**Hugo:** How could that be? Isn’t that contradictory, believing that you don’t know if he exists or not, and believing that he does?

**Alice:** While I have reasons to doubt it most fundamentally, I have much stronger justification for believing in Mr. K’s existence.

**Hugo:** Most intriguing. I see that if knowledge involves truth, the only way we’d know things is if we don’t doubt the knowledge, and that it is true.

**Alice:** Otherwisely put, a proposition is known to us if we don’t doubt it, and it is true.

**Hugo:** How do we know if it is true, though? If we don’t know that our undoubtful belief is true, we can’t be sure that it is indeed knowledge.

**Alice:** Why not consider an example? What about matters like \\(1+1=2\\) where we can surely know it is true? As you mentioned, I don’t doubt this, and it seems obviously true.

**Hugo:** Good observation. I suppose in this case, since we can know that it is true, this can be knowledge.

**Alice:** Alright, another thing to add to the list!

**Hugo:** However, it is true in a different sense: a truth defined by mathematics. The entire truth of the matter is in our heads!

<figure>
{% marginnote() %}Heinrich Vogtherr the Younger. <i>Augsburg Book of Miracles</i>. 16th century.{% end %}
<img src="/essays/kaninchente/miracles.jpg" />
</figure>

### Location of Truth: The Barrier of Experience
**Alice:** Otherwisely put, in our experience. I see.

**Hugo:** Absolutely. I’d say we can only know of knowledge whose truth is determined within our experience.

**Alice:** On the other hand, knowledge whose truth is external to experience is unknowable?

**Hugo:** I don’t know if I’d go that far. That could be problematic. At minimum, we don’t know it.

**Alice:** Oh, right! If we said that we couldn’t know anything to be knowledge other than knowledge whose truth is determined by experience, that itself would be a claim of knowledge about knowledge external to experience. It’s self-refuting.

**Hugo:** Quite astute.

**Alice:** Thank you.

**Hugo:** Of course. Now, to consider a truth of the external variety: when I say that this pond contains water, I am appealing to a truth of the matter external to me, right?

**Alice:** Right.

**Hugo:** I can’t really know that, however, since I can doubt that it contains water.

**Alice:** I’ve got an observation that will muddy the water.

**Hugo:** Really? What is it?

**Alice:** If you define “water” to be “that which is in the pond,” don’t you know that the pond has water? This is still a truth external to experience, is it not?

**Hugo:** Yes, but I don’t think you’d want to say that—see, what if there is nothing in the pond at all?

**Alice:** Oh, I didn’t think of that. But I’m sure some amendment could be made to hammer the same nail.

**Hugo:** There too will be trouble. Even if there had to be something there, by defining water to be what is in the pond, my assertion would be completely different.

**Mr. Kaninchente:** What do you mean by different?

**Hugo:** I would no longer be talking about the water of my experience, so while perhaps I know this to be true, it doesn’t mean anything to me.

**Alice:** I see. Experience must be crucial to this picture. And I am pretty certain about my experience—it would be quite troubling if I can’t even know that.

**Hugo:** You can’t doubt it, that’s for sure. I suppose all the examples we’ve seen where the truth of the matter lies outside experience, but is still meaningful inside experience, I, for one, could doubt.

**Alice:** Right. We agree that either of us cannot even imagine *not* experiencing. Or, for that matter, not existing, in some shape or form.

**Hugo:** Yes.

**Alice:** Of course, I could not do anything, including experiencing, if there were no I.

**Mr. Kaninchente:** I think, therefore I am—Descartes{% sidenote() %}Descartes, René. *Discourse on Method and the Meditations*. Translated 
by F. E. Sutcliffe, Penguin Books Ltd, 1968.
This line appears in Discourse 4, page 53 of my edition.{% end %}.

**Hugo:** Yes, but can I really be sure that I know that I exist? How do I know that it is true that I exist? Just because I don’t doubt it,and don’t see how I could, can I really say that it must be true?

**Mr. Kaninchente:** All of this talk seems quite obtuse. Bringing it down to earth, what does this say about what I am?

**Alice:** It means, like the example with the pond, that if we were to define the concepts of rabbits or ducks such that they apply to you tautologically, we would have a hard time relating that meaningfully to our usual concepts of rabbits and ducks.

**Mr. Kaninchente:** Okay. This would be like saying “rabbits are what I am,” but this wouldn’t really help in the way I was seeking—I wouldn’t want that.

**Alice:** Quite so. If, on the other hand, we were to somehow find a perfect definition of “rabbit,” we could easily test that definition on you to see if it applies. Same with “duck.”

**Mr. Kaninchente:** Oh, I see.

**Hugo:** Not so fast. Even in this case, how could we *easily* test you for the definition?

**Alice:** Well, maybe not easily-

**Hugo:** Say the definition requires that you have two ears. How could we be sure that you do? In this case we would have to test that as well, using another definition that presumably depends on other tests, and so on, until it results in something we know for sure.

**Alice:** Huh. Seems like we have to begin with some knowledge in order to test the definitions, if we had them.

**Hugo:** Yes, but that knowledge couldn’t be of something within our experience, since if it were, it wouldn’t be about Mr. K.

**Alice:** Doubtlessly.

**Hugo:** However, if it were from outside the experience, it wouldn’t really mean much to us.

**Alice:** True, but only if the external knowledge we could have are those which are uninformative, like the pond containing whatever the pond contains.

**Mr. Kaninchente:** This, if not clearly indicative that knowledge is largely impossible to know, demonstrates that attempting it is quite terrible: it makes no sense to me that I am not talking to you two.

**Hugo:** I might agree.

**Mr. Kaninchente:** I’d rather listen again to both of your understandings and use those to develop my own. I won’t be sure that it’s true, but I’d have very good reason to believe it.

**Alice:** Certainly.

**Hugo:** But if we let go of truth, as in being correct, don’t we also get rid of being wrong? Some things are obviously wrong. Never mind doing unjust harm, this simple equation \\(1=2\\) is simply wrong.

**Alice:** And \\(1=1\\) is simply correct—as long as we agree what \\(1\\) and \\(=\\) mean. We can interface with truths defined within our experience. It is just the external truths that Mr. K and I are starting to dislike.

**Hugo:** Yes, I suppose. We’ll have to talk about morality another time.

**Alice:** In any case, what about things we want to talk about without knowledge of them?

<figure>
{% marginnote() %}Camille Flammarion. <i>L'atmosphere: meteorologie populaire</i>. Paris 1888.{% end %}
<img src="/essays/kaninchente/peering.jpeg" />
</figure>

## Turning to Understanding
**Hugo:** Like conjecture? I’d like to hear more.

**Alice:** To take an example, I think it will rain soon, but I also acknowledge it might not. I am not sure of either option, therefore I don’t know either one. As we were saying before, I assign a pretty high credence to my belief that it will rain.

**Hugo:** I see, unless the credence is absolute, the belief wouldn’t be knowledge. We live with all sorts of those beliefs.

**Alice:** Right… actually, we’ve never really relied on external truth in this conversation, but only on understanding, which is justified belief. By testing and otherwise developing our understanding, we conceive of ideas of truth, ideas of knowledge, and even ideas of understanding.

**Hugo:** Oh, I see. I think you are right, truth is somehow less fundamental than understanding.

**Alice:** In the same sense that language is more fundamental than physics, to us, since we need language to do any physics.

**Hugo:** I’d like to seek clarification from your own understanding: I know what a belief is, but what exactly counts as justification?

## Justification and Support
**Alice:** A justification of a belief is any belief that provides support to the justified one. When the justifications are reasonable enough to convince their holder, we can call the combination understanding.

**Hugo:** Of course. I can’t think of a better alternative. Let’s see… when I believe that I saw a rain cloud, and I believe that seeing rain clouds often means it will rain, and believe that those are sufficient reasons to believe that it will rain, I understand that it will rain. Regardless of it actually being the case or not.

**Alice:** Right.

**Mr. Kaninchente:** Since I’ve heard people call me all sorts of things, but don’t see them compelling enough to classify myself as rabbit, duck, or some permutation of both, I don’t really understand myself as any of those. I’m satisfied with this answer. I’m me, no need to think about it too hard.

**Alice:** I’m glad you’ve come to some understanding of yourself, Mr. K.

**Mr. Kaninchente:** *Danke schön*, Alice, Hugo, you have been most helpful.

**Alice:** Thank you for your provocative questions!

## Clarification
**Hugo:** Alice, what about languages? If you can understand German, what is the belief, and what is the justification?

**Alice:** That’s different. We do say that we understand certain things, but the understanding that is *justified belief* only applies when we do have a belief of the form of an assertion, which could be assigned a truth value.

**Hugo:** Like “Mr. K is a rabbit,” but not like “Happy Birthday!”

**Alice:** Absolutely. Likewise, a language, as a whole, can’t be true or false.

**Hugo:** I see. Then we don’t mean this understanding when understanding German. Curiously, we do when we understand that we understand German.

**Mr. Kaninchente:** Oh, that makes sense.

**Hugo:** But then what is development of understanding? If we don’t have a truth that we can use to measure progress, this is a vitally important matter. How would development of understanding work?

## Development of Understanding
**Alice:** Let’s see. Development involves change, and it should somehow be positive—but how could a change be considered positive without considering truth?

**Hugo:** Well, we’ve got three elements to consider: credence, justification, and support. Why don’t we go with credence?

**Alice:** Why should we? We should think it through and establish reasons. If we don’t, it couldn’t be understanding. That’s it! So long as support for beliefs has a net increase, any change is development. Gosh, I am so smart.

**Hugo:** That formulation makes sense. To delve a bit deeper, then, what kinds of changes are possible where support is increased?

**Alice:** I can find a few. If I gained new evidence for a belief, like that Mr. K enjoys carrots, that will be more support for him being a rabbit. This could be called *rooting*, making foundations more stable by connecting a belief with more support.

**Hugo:** I concur! Also, you just went the other way: you slowly ventured through your understanding and created some insight.

**Alice:** Insight is the belief I justified, that wasn’t there at first, right?

**Hugo:** You are most correct.

**Alice:** So what should this type of development be called?

**Mr. Kaninchente:** Since it arose from the combination of prior beliefs, how about calling this *synthesis*?

**Alice:** I like it. Rooting and synthesis.

**Hugo:** I've noticed something we've missed. It is quite crucial to this picture!

**Alice:** What is it?

**Hugo:** How do we get beliefs in the first place? I don't mean those through synthesis, but before they are even understanding?

**Alice:** Quite an oversight! Well, taking a step back, what you have just said developed my understanding.

**Hugo:** How so?

**Alice:** Well, first, I must have gotten some belief of what you said. Through synthesis and rooting, I formed a network of understanding that found it a credible belief that *forming beliefs* must be understood.

**Hugo:** Wow! That is quite lucid of you. It seems like the process was triggered by a new belief that wasn't synthesized through understanding. It just appeared to you.

**Alice:** That's exactly what it felt like, in retrospect.

**Mr. Kaninchente:** Could you explore this a bit more?

**Alice:** Of course! I suppose I generated this belief of what you said based on my sense data. It's not that I consciously chose to; it all happened subconsciously.

**Mr. Kaninchente:** A sensible sentiment.

**Hugo:** Perplexing! What to call this?

**Alice:** Call it *sensing*. After all, it was simply what my body came to believe through sense experience alone—even if I didn't believe you exist, at some level I would have the belief that you said those things.

**Hugo:** That seems right to me, but it is really just a name.

**Mr. Kaninchente:** The names aren't so important. The concepts should take center stage.

**Alice:** I think so too, but it is good nonetheless to have clear terminology. Also, you implied that it was clear how these beliefs are formed through synthesis. It's not simple to me.

**Hugo:** How so? Two or more beliefs combine to form a new one.

**Alice:** Yes, but how do they do so?

**Hugo:** The brain constantly tries to make sense of things. It tries to make connections between concepts and ideas. It seems to me that synthesis is one type of this connection-building.

**Alice:** Most clever. I agree wholeheartedly.

**Mr. Kaninchente:** Now you've made me curious! I would like to, on another occasion, dive deeper into this.

**Alice:** I concur! Mr. K, do you see that cottage?

**Mr. Kaninchente:** Yes, as I understand it! What about it?

**Alice:** That is our destination---a symbol that our conversation must soon come to an end. Is there anything we've neglected?

<figure>
{% marginnote() %}Caillebotte, Gustave. <i>Chemin Montant</i>. oil on canvas, 1881. (cropped){% end %}
<img src="/essays/kaninchente/chemin-montant.jpg" />
</figure>

# Reaching Home
**Hugo:** What about when we, for lack of a better word, destroy old support in favor of new ones?

**Alice:** Hmm. Destruction of support, of course, does not increase understanding on its own. However, if we destroy support for a reason, this would be like adding anti-support for what we believed, and support for not believing it.

**Hugo:** I can somewhat see. It may help to find a way to diagram these things.

**Alice:** I agree. Let us do that sometime by the blackboard.

**Hugo:** Great. But then what about simply increasing credence of justification?

**Alice:** You mean, “is increasing credence development of understanding?”

**Hugo:** Right. And if it isn’t, I still feel like it should be.

**Alice:** I say it isn’t. What are the reasons for the new credence? If there are some, this is rooting, or is that incorrect?

**Hugo:** That’s correct.

**Alice:** If there aren’t, how is this change considered “positive” in any sense?

**Hugo:** Well, of course it’s positive: you are increasing reason to believe if you increase credence of a reason.

**Alice:** The problem I have is that if the increase in credence isn’t justified. The increase is arbitrary.

**Hugo:** I am beginning to see your point.

**Alice:** Yes. I was wrong in my previous assertion of development.

**Hugo:** How so?

**Alice:** In development of understanding, some new supporting link *must* be made. Merely learning facts doesn’t cut it, and just 
increasing credence doesn’t either.

**Hugo:** I’ll have to think about it more on my own, but I think you are correct.

**Alice:** Great. In any case, we’ve reached our destination.

**Hugo:** Quite so. Mr. Kaninchente, would you like to join us for brunch?

**Mr. Kaninchente:** Of course! So long as I do not impose.

**Alice:** No, not at all. It would be our pleasure to treat our new friend.

**Hugo:** Let us put this discussion to rest, and talk again soon---at that point let us discuss the various ways we develop understanding, at a level higher up than simple rooting, synthesis, and sensing.

**Alice:** Perhaps we will address the issue of morality you raised, or a way to diagram understanding.

**Hugo:** Oh, good. Mr. Kaninchente, do you like Borscht?

---

<small>All artwork is public domain.</small>