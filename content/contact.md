+++
title = "Contact & Links"
template = "page.html"
aliases = ["c"]
+++

## Messaging and Online Accounts

Please feel free to contact me with any medium you prefer. Introduce yourself when you do so in order to prevent coming across as spam.

Message me through [:email](#Email), [:matrix](#Matrix), or [:LinkedIn](#LinkedIn). If you have contact with me through some other platform, that is also fine! Find out what I'm reading at [:LibraryThing](#LibraryThing).

## Email

The easiest way to contact me is through email. My personal email is `louis [at] birla.io`. {% marginnote() %}If you want to send me an email securely, you can use my <a href="https://keys.openpgp.org/vks/v1/by-fingerprint/0F5BA56CBA6418D6A76902FCE96BDE841E048EA7" download>pgp key</a>.If this one is out of date or unavailable, let me know and I will update it.{% end %}

I may change mailservers in the future, if that affects you.

## LinkedIn

While I still prefer professional communication through email, [LinkedIn](https://www.linkedin.com/in/louisbirla/) works as well.

## Matrix

You can find me on [Matrix](https://matrix.org) at the address `@louisbirla:matrix.org`. I don't have a client opened, but will check the application occasionally. This is entirely because I do not have anybody to talk to with Matrix.

## LibraryThing

Adding me as a friend or finding me important on [LibraryThing](https://www.librarything.com/profile/louisbirla) is a good way to spy on what I am reading, recomend books, and many more features I do not know about!

I used to use Goodreads for this, as well as Inventaire, but found LibraryThing's feature set *much* better. I specifically chose it because of the recommendation system, but there are countless beautiful features.

## Fediverse

### Mastodon

I am on Mastodon as [@louisbirla@indieweb.social](https://indieweb.social/@louisbirla).

## GitHub

My account is [louisbirla](https://github.com/louisbirla).

## Donations

If you want to donate to me for some reason, please do so through [liberapay](https://liberapay.com/louisbirla/).
